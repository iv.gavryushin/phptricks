<?php

//Matrix for rotation
$direction_x = array(
    0,
    10,
    10,
    10,
    0, -10, -10, -10
);
$direction_y = array(
    10,
    10,
    0, -10, -10, -10,
    0,
    10
);

//Check for vars  
if (!isset($a) && !isset($b))
{
    $a = 0;
    $b = 0;
}

if (!isset($x) && !isset($y))
{
    $x = 0;
    $y = 10;
    $i = - 1;
}

//Rotation here
$i = $_GET['i'];

if (isset($_GET['rotate_clockwise']))
{
    if ($i == 7) $i = - 1;
    $i++;
    $x = $direction_x[$i];
    $y = $direction_y[$i];
}

if (isset($_GET['rotate_counter_clockwise']))
{
    if ($i == 0) $i = 8;
    $i--;
    $x = $direction_x[$i];
    $y = $direction_y[$i];
}

//Move control
if (isset($_GET['forward']))
{
    if ($_GET['i'] == '') $i = 0;
    $x = $_GET['x'];
    $y = $_GET['y'];

    //for direct first ray
    $a = $_GET['a'] + ($direction_x[$i]);
    $b = $_GET['b'] + ($direction_y[$i]);

}
else
{
    $a = $_GET['a'];
    $b = $_GET['b'];

}

//This is x,y of a wall we inspect
$wall_x = 150;
$wall_y = 300;


//Ray intersection
$vecx = $x / 10;
$vecy = $y / 10;
$wallhight = 100;

//Raycasting as a cycle. 100 is a  horizon point.
//We have only one ray here
for ($i2 = 0;$i2 != 100;$i2++)
{
    $vecx += $x / 10;
    $vecy += $y / 10;

    if ($vecx + 210 + $a == $wall_x && $vecy + 310 + $b == $wall_y)
    {

        echo '<div id="wall_real" style="height: ' . (200 - $wallhight - $i2) . 'px;  left: 10px;"></div>';
    }
}
unset($vecx, $vecy);



?>
<style>
			#point_of_view {
			position: absolute;
			top:  <?=$x + 210 + $a ?>px;
			left: <?=$y + 310 + $b ?>px;
			background-color: #999;
			width: 5px;
			height: 5px;
			}	
		
			#point_zero {
			position: absolute;
			top:  <?=$a + 210 ?>px;
			left: <?=$b + 310 ?>px;
			background-color: #333;
			width: 5px;
			height: 5px;
			}
			#wall {
			position: absolute;
			top:  <?=$wall_x ?>px;
			left: <?=$wall_y ?>px;
			background-color: blue;
			width: 5px;
			height: 5px;
			}
			#wall_real {
			position: absolute;
			bottom:  315px;
			left: 30px;
			background-color: blue;
			width:  10px;
			height: 10px;
			}		
</style>


<div id="wall">
</div>
<div id="point_of_view">
</div>
<div id="point_zero">
</div>


<br><br><br><br><br>
<form  method="GET" align="left">
<input type="submit" name="rotate_clockwise"  value="rotate_clockwise">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
</form>

<form  method="GET" align="left">
<input type="submit" name="rotate_counter_clockwise"  value="rotate_counter_clockwise">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
</form>

<form  method="GET" >
<input type="submit" name="forward" value="move">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">

</form>
